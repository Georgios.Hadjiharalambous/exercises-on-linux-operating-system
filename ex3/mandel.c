/*
 * mandel.c
 *
 * A program to draw the Mandelbrot Set on a 256-color xterm.
 *

 */
#include <signal.h> 
#include <pthread.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <semaphore.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <errno.h>
#include "mandel-lib.h"

#define MANDEL_MAX_ITERATION 100000


/***************************
 * Compile-time parameters *
 ***************************/

/*
 * Output at the terminal is is x_chars wide by y_chars long
*/
int y_chars = 50;
int x_chars = 90;

/*
 * The part of the complex plane to be drawn:
 * upper left corner is (xmin, ymax), lower right corner is (xmax, ymin)
*/
double xmin = -1.8, xmax = 1.0;
double ymin = -1.0, ymax = 1.0;
	
/*
 * Every character in the final output is
 * xstep x ystep units wide on the complex plane.
 */
double xstep;
double ystep;

int N;
sem_t **sema;//a= malloc(sizeof(sem_t));
//sem_init(sema , 0 , 1);


/*
 * This function computes a line of output
 * as an array of x_char color values.
 */
void compute_mandel_line(int line, int color_val[])
{
	/*
	 * x and y traverse the complex plane.
	 */
	double x, y;

	int n;
	int val;

	/* Find out the y value corresponding to this line */
	y = ymax - ystep * line;

	/* and iterate for all points on this line */
	for (x = xmin, n = 0; n < x_chars; x+= xstep, n++) {

		/* Compute the point's color value */
		val = mandel_iterations_at_point(x, y, MANDEL_MAX_ITERATION);
		if (val > 255)
			val = 255;

		/* And store it in the color_val[] array */
		val = xterm_color(val);
		color_val[n] = val;
	}
}

/*
 * This function outputs an array of x_char color values
 * to a 256-color xterm.
 */
void output_mandel_line(int fd, int color_val[])
{
	int i;
	
	char point ='@';
	char newline='\n';

	for (i = 0; i < x_chars; i++) {
		/* Set the current color, then output the point */
		set_xterm_color(fd, color_val[i]);
		if (write(fd, &point, 1) != 1) {
			perror("compute_and_output_mandel_line: write point");
			exit(1);
		}
	}

	/* Now that the line is done, output a newline character */
	if (write(fd, &newline, 1) != 1) {
		perror("compute_and_output_mandel_line: write newline");
		exit(1);
	}
}

void compute_and_output_mandel_line(int fd, int line)
{
	/*
	 * A temporary array, used to hold color values for the line being drawn
	 */
	int color_val[x_chars];

	compute_mandel_line(line, color_val);
	output_mandel_line(fd, color_val);
}

void *sync1 (void *arg) {
	int *k;
	k = arg;
	int i = *k;
	int j;
	j = i;
	int color_val[x_chars];
	while (i < y_chars) {
		compute_mandel_line(i, color_val);
		int v = sem_wait(sema[i%N]);
		if (v!=0) printf ("error in thread");
		output_mandel_line (1, color_val);
		i += N;
		j=(i+1)%N;
		sem_post(sema[j]);	
	} 
	return NULL;
}


void handle_signal(int signal) {
	int i;
	reset_xterm_color(1);
	for(i=0; i<N; i++)
		free(sema[i]);
	free(sema);
	printf("\nSIGINT signaled, or ctrl+C pressed \n");
	exit(0);
}


int main (int args, char **argc) {

	if (signal(SIGINT, handle_signal) == SIG_ERR)
 		printf("\ncan't catch SIGINT\n");

	int  val;
	int i, Retval,k;
	
	N = atoi(argc[1]);
	xstep = (xmax - xmin) / x_chars;
	ystep = (ymax - ymin) / y_chars;
	sema = (sem_t**)malloc(N*sizeof(*sema));
	for (k=0; k<N; k++)
		sema[k] = (sem_t*) malloc(sizeof(sem_t));
	val = sem_init (sema[0], 0, 1);
	if (val!=0) printf ("error in sem_init\n");
	for (i=1; i<N; i++) {
		val = sem_init (sema[i], 0, 0);
		if (val!=0) printf ("error in sem_init\n");
	}

	pthread_t thr[N];
	int a[N];
	i = 0;
	while (i < N){
		a[i]=i;		
		Retval = pthread_create(&thr[i], NULL, sync1, &a[i]);
		if(Retval!=0) printf("problem file m\n");
		i++;
	}

	int p0,pp;
	pp=0;
	for (p0=0; p0<N; p0++){
	pp = pthread_join(thr[p0],NULL);
	if(pp!=0) printf("error in pthrea_join n");
	}
	reset_xterm_color(1);
	for(i=0;i<N;i++)
	free(sema[i]);
	free(sema);
	return 0;
}

