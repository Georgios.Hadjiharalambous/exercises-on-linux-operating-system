Projects using Linux operating system. Use of processes and threads, semaphores and locks. <br/>
Created during operating systems course at NTUA ECE. <br/>
Developed by [Myrsini Vakalopoulou ](https://gitlab.com/mdvakalop) and [Georgios Hadjiharalambous](https://gitlab.com/Georgios.Hadjiharalambous)