#include <stdio.h>
#include <stdlib.h>
#include "proc-common.h"
#include "tree.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

//creates tree from file

void create_ptree (struct tree_node *root) {
	pid_t p[root->nr_children];
	int status,i;
	printf("name of root %s\n",root->name);
	if (root -> children == NULL || root -> nr_children == 0) {
		sleep(5);
		exit(1);
	}
	for ( i=0; i < root->nr_children; i++) {
		p[i] = fork();
		if (p[i]<0) {
			perror("create_ptree: fork");
			exit(0);
		}		
		if (p[i]==0) {
			change_pname((root->children + i)->name);
			create_ptree(root->children + i);
		}
	}
	//sleep(1);
	for(i=0;i<root->nr_children;i++) { 	//αφου βγει απο το λοοπ πχ η Β (θα εχει δημιουργησει τα 
		p[i] = wait(&status);		//2 παδια της που κανουν σλιπ) κανει 2 γουειτ
		explain_wait_status(p[i], status);
	}
	exit(2);
}


int main(int argc, char *argv[])
{
	struct tree_node *root;
	pid_t p;
	int status;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <i nput_tree_file>\n\n", argv[0]);
		exit(1);
	}

	root = get_tree_from_file(argv[1]);
	print_tree(root);
	
	p = fork ();
	if (p<0) {
		perror("main: fork");
		exit(1);
	}
	if (p==0) {
		change_pname(root->name);
		create_ptree(root);
		exit(1);
	}
	show_pstree(p);
	p = wait(&status);
	explain_wait_status(p, status);
	
	return 0;
}
