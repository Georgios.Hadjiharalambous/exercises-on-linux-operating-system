
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "proc-common.h"

#define SLEEP_PROC_SEC  5
#define SLEEP_TREE_SEC  3

/*
 * Create this process tree:
 * A-+-B---D
 *   `-C
 */


void childB(){
	pid_t pD;
	int status;
	
	fprintf(stderr, "Parent B, PID = %ld: Creating child D...\n",
                (long)getpid());
	pD=fork();
	if (pD < 0) {
                /* fork failed */
                perror("fork");
                exit(1);
        }
        if (pD == 0) {
                /* In child process */
		printf ("D is awake.\n");
                change_pname("D");
		printf("D: Sleeping...\n");
                sleep(5);    
      		exit(13);
	}
	printf("Parent B, PID = %ld: Created child D with PID = %ld, waiting for it to terminate...\n",
                (long)getpid(), (long)pD);

        pD = wait(&status);
       	explain_wait_status(pD, status);

        printf("ParentB: All done, exiting...\n");
}


void fork_procs(void)
{
	pid_t pB,pC;
	int status;
	change_pname("A");

	fprintf(stderr, "Parent A, PID = %ld: Creating child B...\n",
		(long)getpid());
	pB = fork();
	if (pB < 0) {
		/* fork failed */
		perror("fork");
		exit(1);
	}
	if (pB == 0) {
		/* In child process */
		printf ("B is awake.\n");
		change_pname("B");
		childB();
		exit(19);
	}
	
	fprintf(stderr, "Parent A, PID = %ld: Creating child C...\n",
     	  (long)getpid());
	
	pC = fork();
	if (pC < 0) {
                /* fork failed */
                perror("fork");
                exit(1);
        }
        if (pC == 0) {
               /* In child process */
		printf ("C is awake.\n");
                change_pname("C");
              	sleep(5);
                exit(17);
        }
	printf("Parent A, PID = %ld: Created child B with PID = %ld, waiting for it to terminate...\n",
                (long)getpid(), (long)pB);
	 printf("Parent A, PID = %ld: Created child C with PID = %ld, waiting for it to terminate...\n",
                (long)getpid(), (long)pC);
	
	/*
	 * In parent process. Wait for the child to terminate
	 * and report its termination status.
	 */
	
	pC = wait(&status);
	explain_wait_status(pC, status);	
	
        pB = wait(&status);
        explain_wait_status(pB, status);

	printf("ParentA: All done, exiting...\n");
	exit(16);
}

/*
 * The initial process forks the root of the process tree,
 * waits for the process tree to be completely created,
 * then takes a photo of it using show_pstree().
 *
 * How to wait for the process tree to be ready?
 * In ask2-{fork, tree}:
 *      wait for a few seconds, hope for the best.
 * In ask2-signals:
 *      use wait_for_ready_children() to wait until
 *      the first process raises SIGSTOP.
 */
int main(void)
{
	pid_t pA;
	int status;
	pA = fork ();
	if (pA<0) {
		perror("main:fork");
		exit(1);
	}
	if (pA==0) {
		printf ("A is awake.\n");
		fork_procs();
		exit(1);
	}

	/*
	 * Father
	 */
	/* for ask2-signals */
	/* wait_for_ready_children(1); */

	/* for ask2-{fork, tree} */
	sleep(SLEEP_TREE_SEC);

	/* Print the process tree root at pid */
	show_pstree(pA);

	/* for ask2-signals */
	/* kill(pid, SIGCONT); */

	/* Wait for the root of the process tree to terminate */
	pA = wait(&status);
	explain_wait_status(pA, status);

	return 0;
}
