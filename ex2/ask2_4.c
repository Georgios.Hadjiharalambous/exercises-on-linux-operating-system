#include <stdio.h>
#include <stdlib.h>
#include "proc-common.h"
#include "tree.h"
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

//creates tree and computes result using sleep and pipes

void create_ptree (struct tree_node *root, int *fd) {
	pid_t p[root->nr_children];
	int status,i;
	long int a;

	printf("PID = %ld, name %s, starting...\n",
			(long)getpid(), root->name);
	change_pname(root->name);
	close(fd[0]); //fd is only for write in this func	
	
	if (root -> children == NULL || root -> nr_children == 0) {
		a=strtol(root->name,NULL,10);
		if (write (fd[1],&a, sizeof(a)) != sizeof(a)) {
			perror ("parent: write to pipe\n");
			exit (4);
		}	
		sleep(1);
		exit(1);
	}
	int result;
	int pfd[2];
	int data[2];
	close (pfd[1]); //pfd in only for read in this func	
	
	for ( i=0; i<2; i++) {
		if (pipe(pfd)<0) {
			perror ("pipe");
			exit(5);
		}

		p[i] = fork();
		if (p[i]<0) {
			perror("create_ptree: fork");
			exit(0);
		}		
		if (p[i]==0) {
			
			create_ptree(root->children + i, pfd);	 
		}
		if (read (pfd[0], &data[i], sizeof(int)) != sizeof(int)) {
			perror ("child: read from pipe");
			exit (4);
		}
	}
	
	if (!strcmp(root->name, "+")) result=data[0] + data[1];
	else result=data[0]*data[1];
	
	
	if (write (fd[1], &result, sizeof(result)) != sizeof(result)) {
			perror ("parent: write to pipe\n");
			exit (4);
	}
	for(i=0;i<root->nr_children;i++) { 	//αφου βγει απο το λοοπ πχ η Β (θα εχει δημιουργησει τα 
		p[i] = wait(&status);		//2 παδια της που κανουν σλιπ) κανει 2 γουειτ
		explain_wait_status(p[i], status);
	}
	
	exit(2);
}


int main(int argc, char *argv[])
{
	struct tree_node *root;
	pid_t p;
	int status;
	int pfd[2];
	int res;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <i nput_tree_file>\n\n", argv[0]);
		exit(1);
	}

	root = get_tree_from_file(argv[1]);
	print_tree(root);
	
	if (pipe(pfd)<0) {
		perror ("pipe");
		exit(5);
	}
	
	p = fork ();
	if (p<0) {
		perror("main: fork");
		exit(1);
	}
	if (p==0) {
		change_pname(root->name);
		create_ptree(root, pfd);
		exit(1);
	}

	show_pstree(p);
	p = wait(&status);
	explain_wait_status(p, status);

	if (read (pfd[0], &res, sizeof(int)) != sizeof(int)) {
		perror ("child: read from pipe");
		exit (4);
	}
	
	close(pfd[0]);
	close(pfd[1]);
	printf("result= %d\n", res);	
		
	return 0;
}
